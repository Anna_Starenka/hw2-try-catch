//  try...catch - дозволяє нам «виловлювати» помилки, щоб код міг продовжувти далі виконуватися, якщо сталася якась незначна помилка (несподіваний вхід користувача, помилкова   відповідь сервера)



const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];


const div = document.getElementById("root");
const ul = document.createElement("ul");
div.prepend(ul);

const findProp = ["author", "name", "price"];

books.forEach((element) => {
  try {
    if (findProp.every((prop) => element.hasOwnProperty(prop))) {
      let li = document.createElement("li");
      li.innerHTML = `Author: ${element.author}<br>Name: ${element.name}<br>Price: ${element.price}`;
      ul.appendChild(li);
    } else {
      const missingProp = findProp.find(
        (prop) => !element.hasOwnProperty(prop)
      );
      throw new Error(`Property is absent "${missingProp}"`);
    }
  } catch (error) {
    console.error(error.message);
  }
});
